package com.shopback.interviewassignment.util;

import android.util.Log;

import rx.functions.Action1;

public final class Logger {
    private Logger() {
        super();
    }

    public static void log(String message) {
        Log.d("OVO Deals", message);
    }

    public static Action1<Throwable> logException() {
        return Logger::logException;
    }

    public static void logException(final Throwable throwable) {
        throwable.printStackTrace();
    }

    public static void logHttp(String message) {
        Log.i("Http", message);
    }
}