package com.shopback.interviewassignment.util;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.shopback.interviewassignment.CoreApplication;

import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingConversion;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class BindingAdapters {
    @BindingAdapter(value = {"imageUrl"}, requireAll = false)
    public static void bindImageUrl(ImageView imageView, String imageUrl) {
        Context context = CoreApplication.getInstance();
        if (context != null) {
            Glide.with(context)
                    .load("https://image.tmdb.org/t/p/original" + imageUrl)
                    .into(imageView);
        }
    }

    @BindingAdapter(value = {"refreshing", "onRefreshListener"}, requireAll = false)
    public static void bindRefreshing(SwipeRefreshLayout swipeRefreshLayout, boolean refreshing, SwipeRefreshLayout.OnRefreshListener listener) {
        swipeRefreshLayout.setRefreshing(refreshing);
        if (listener != null) {
            swipeRefreshLayout.setOnRefreshListener(listener);
        }
    }

    @BindingConversion
    public static int convertBooleanToVisibility(Boolean value) {
        return value ? View.VISIBLE : View.GONE;
    }
}