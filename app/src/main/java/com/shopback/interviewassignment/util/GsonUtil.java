package com.shopback.interviewassignment.util;

import com.shopback.interviewassignment.network.api.pojo.response.GsonAdaptersMovieListResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shopback.interviewassignment.vivmer.entity.GsonAdaptersGenre;
import com.shopback.interviewassignment.vivmer.entity.GsonAdaptersMovieDetail;
import com.shopback.interviewassignment.vivmer.entity.GsonAdaptersMovieList;

public class GsonUtil {
    private static Gson GSON;

    private GsonUtil() {
        // Ensure Singleton
    }

    public static synchronized Gson getGson() {
        if (GSON == null) {
            final GsonBuilder gsonBuilder = new GsonBuilder();

            //POJO
            gsonBuilder.registerTypeAdapterFactory(new GsonAdaptersMovieListResponse());

            //MODEL
            gsonBuilder.registerTypeAdapterFactory(new GsonAdaptersMovieList());
            gsonBuilder.registerTypeAdapterFactory(new GsonAdaptersMovieDetail());
            gsonBuilder.registerTypeAdapterFactory(new GsonAdaptersGenre());

            GSON = gsonBuilder.create();
        }
        return GsonUtil.GSON;
    }
}
