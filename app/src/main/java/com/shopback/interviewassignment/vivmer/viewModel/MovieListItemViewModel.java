package com.shopback.interviewassignment.vivmer.viewModel;

import android.view.View;

import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

public interface MovieListItemViewModel extends RecyclerViewItemViewModel {
    String getImageUrl();

    String getTitle();

    String getPopularity();

    void onItemClicked(View view);
}