package com.shopback.interviewassignment.vivmer.view;

import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.shopback.interviewassignment.CoreApplication;
import com.shopback.interviewassignment.R;
import com.shopback.interviewassignment.dagger.module.MovieDetailModule;
import com.shopback.interviewassignment.databinding.ActivityMovieDetailBinding;
import com.shopback.interviewassignment.vivmer.entity.BaseMovie;
import com.shopback.interviewassignment.vivmer.interactor.MovieDetailInteractor;
import com.shopback.interviewassignment.vivmer.router.MovieListRouter;
import com.shopback.interviewassignment.vivmer.viewModel.MovieDetailViewModel;
import com.shopback.interviewassignment.vivmer.viewModel.MovieDetailViewModelImpl;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;

public class MovieDetailActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener {
    @Inject
    MovieDetailInteractor mInteractor;

    private ActivityMovieDetailBinding mBinding;
    private BaseMovie                  mBaseMovie;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_movie_detail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreApplication.getInstance()
                .getApplicationComponent()
                .plus(new MovieDetailModule(this))
                .inject(this);
        getInputData();
        bindData();
        setup();
    }

    @Override
    public void onOffsetChanged(final AppBarLayout appBarLayout, final int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;
        handleToolbar(percentage);
    }

    private void getInputData() {
        mBaseMovie = getIntent().getParcelableExtra(MovieListRouter.EXTRA_MOVIE);
    }

    private void bindData() {
        final MovieDetailViewModel viewModel = new MovieDetailViewModelImpl(this, mInteractor, mBaseMovie);
        mBinding = DataBindingUtil.setContentView(this, getLayoutResource());
        mBinding.setViewModel(viewModel);
    }

    private void setup() {
        setSupportActionBar(mBinding.actionBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        mBinding.appBarLayout.addOnOffsetChangedListener(this);
        handleToolbar(0f);
    }

    private void handleToolbar(float percentage) {
        mBinding.actionBar.getBackground().setAlpha((int) (percentage * 255));
        mBinding.collapsingToolbarLayout.setTitle(percentage == 1.0f ? mBaseMovie.title() : "");
    }
}