package com.shopback.interviewassignment.vivmer.router;

import android.content.Intent;

import com.shopback.interviewassignment.vivmer.view.BaseActivity;
import com.shopback.interviewassignment.vivmer.view.MovieListActivity;

public class MovieDetailRouter {
    private final BaseActivity mActivity;

    public MovieDetailRouter(BaseActivity activity) {
        mActivity = activity;
    }

    public void navigateBackMovieList(long movieId) {
        Intent intent = new Intent(mActivity, MovieListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mActivity.startActivity(intent);
    }
}