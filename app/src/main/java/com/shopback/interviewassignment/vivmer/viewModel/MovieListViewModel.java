package com.shopback.interviewassignment.vivmer.viewModel;

import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewViewModel;

public interface MovieListViewModel extends RecyclerViewViewModel {
    void loadMore(int page);
}