package com.shopback.interviewassignment.vivmer.router;

import android.content.Intent;

import com.shopback.interviewassignment.vivmer.entity.MovieList;
import com.shopback.interviewassignment.vivmer.view.BaseActivity;
import com.shopback.interviewassignment.vivmer.view.MovieDetailActivity;

public class MovieListRouter {
    public static final String EXTRA_MOVIE = "EXTRA_MOVIE";

    private final BaseActivity mActivity;

    public MovieListRouter(BaseActivity activity) {
        mActivity = activity;
    }

    public void navigateToMovieDetail(final MovieList movieList) {
        Intent intent = new Intent(mActivity, MovieDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_MOVIE, movieList);
        mActivity.startActivity(intent);
    }
}