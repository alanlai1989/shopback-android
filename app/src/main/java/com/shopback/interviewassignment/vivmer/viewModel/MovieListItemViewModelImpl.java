package com.shopback.interviewassignment.vivmer.viewModel;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.shopback.interviewassignment.R;
import com.shopback.interviewassignment.vivmer.entity.BaseMovie;
import com.shopback.interviewassignment.vivmer.entity.MovieList;
import com.shopback.interviewassignment.vivmer.router.MovieListRouter;

public class MovieListItemViewModelImpl implements MovieListItemViewModel {
    private final Context         mContext;
    private final MovieListRouter mRouter;
    private final BaseMovie       mBaseMovie;

    MovieListItemViewModelImpl(final Context context,
                               final MovieListRouter router,
                               final BaseMovie baseMovie) {
        mContext = context;
        mRouter = router;
        mBaseMovie = baseMovie;
    }

    @Override
    public String getImageUrl() {
        String imageUrl = mBaseMovie.poster_path();
        if (TextUtils.isEmpty(imageUrl)) {
            imageUrl = mBaseMovie.backdrop_path();
        }
        return imageUrl;
    }

    @Override
    public String getTitle() {
        if (mBaseMovie == null) {
            return "";
        }
        return mBaseMovie.title();
    }

    @Override
    public String getPopularity() {
        if(mBaseMovie == null){
            return "";
        }
        return mContext.getString(R.string.popularity, String.valueOf(mBaseMovie.popularity()));
    }

    @Override
    public void onItemClicked(final View view) {
        if (mRouter != null) {
            mRouter.navigateToMovieDetail((MovieList) mBaseMovie);
        }
    }
}