package com.shopback.interviewassignment.vivmer.entity;

import android.os.Parcel;

import com.google.gson.JsonSyntaxException;
import com.shopback.interviewassignment.util.GsonUtil;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import java.util.List;

import androidx.annotation.Nullable;

@Gson.TypeAdapters
@Value.Immutable
public abstract class MovieDetail implements BaseMovie {
    public abstract String overview();

    public abstract List<Genre> genres();

    public abstract String original_language();

    @Nullable
    public abstract Integer runtime();

    // ----- Boilerplate for Parcelable ----- //
    public static final Creator<MovieDetail> CREATOR = new Creator<MovieDetail>() {
        @Override
        public MovieDetail createFromParcel(final Parcel parcel) {
            return fromJsonString(parcel.readString());
        }

        @Override
        public MovieDetail[] newArray(final int i) {
            return new MovieDetail[0];
        }
    };

    @Override
    public void writeToParcel(final Parcel parcel, final int flag) {
        parcel.writeString(toJsonString());
    }

    public String toJsonString() {
        return GsonUtil.getGson().toJson(this, MovieDetail.class);
    }

    public static MovieDetail fromJsonString(final String json) {
        try {
            return GsonUtil.getGson().fromJson(json, MovieDetail.class);
        } catch (final JsonSyntaxException e) {
            return null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}