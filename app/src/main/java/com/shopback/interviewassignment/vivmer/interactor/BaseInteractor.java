package com.shopback.interviewassignment.vivmer.interactor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

class BaseInteractor {
    static final String API_KEY                 = "328c283cd27bd1877d9080ccb1604c91";
    static final String RELEASE_DATE_DESCENDING = "release_date.desc";

    String getTodayDate() {
        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(today);
    }
}