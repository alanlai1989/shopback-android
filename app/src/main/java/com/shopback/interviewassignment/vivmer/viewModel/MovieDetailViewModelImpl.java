package com.shopback.interviewassignment.vivmer.viewModel;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import com.shopback.interviewassignment.R;
import com.shopback.interviewassignment.util.Logger;
import com.shopback.interviewassignment.vivmer.entity.BaseMovie;
import com.shopback.interviewassignment.vivmer.entity.Genre;
import com.shopback.interviewassignment.vivmer.entity.MovieDetail;
import com.shopback.interviewassignment.vivmer.interactor.MovieDetailInteractor;
import com.shopback.interviewassignment.vivmer.view.BaseActivity;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MovieDetailViewModelImpl extends MovieListItemViewModelImpl implements MovieDetailViewModel {
    private final Context               mContext;
    private final MovieDetailInteractor mInteractor;
    private final BaseMovie             mBaseMovie;

    private ObservableBoolean       mInitialVisibility = new ObservableBoolean();
    private ObservableField<String> mOverview          = new ObservableField<>();
    private ObservableField<String> mGenres            = new ObservableField<>();
    private ObservableField<String> mLanguage          = new ObservableField<>();
    private ObservableField<String> mDuration          = new ObservableField<>();

    public MovieDetailViewModelImpl(final Context context,
                                    final MovieDetailInteractor interactor,
                                    final BaseMovie baseMovie) {
        super(context, null, baseMovie);
        mContext = context;
        mInteractor = interactor;
        mBaseMovie = baseMovie;
        fetchMovieDetail();
    }

    @Override
    public ObservableBoolean getInitialVisibility() {
        return mInitialVisibility;
    }

    @Override
    public ObservableField<String> getOverview() {
        return mOverview;
    }

    @Override
    public ObservableField<String> getGenres() {
        return mGenres;
    }

    @Override
    public ObservableField<String> getLanguage() {
        return mLanguage;
    }

    @Override
    public ObservableField<String> getDuration() {
        return mDuration;
    }

    @Override
    public void onBookClicked(final View view) {
        final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/url?q=https://www.cathaycineplexes.com.sg/&sa=D&ust=1575179274532000"));
        mContext.startActivity(browserIntent);
    }

    private void fetchMovieDetail() {
        ((BaseActivity) mContext).showProgressDialog();
        mInteractor.getMovieList(mBaseMovie.id())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movieDetail -> {
                    displayContent(movieDetail);
                    ((BaseActivity) mContext).dismissProgressDialog();
                }, throwable -> {
                    ((BaseActivity) mContext).dismissProgressDialog();
                    Logger.logException(throwable);
                    ((BaseActivity) mContext).finish();
                    Toast.makeText(mContext, "Error while fetching data", Toast.LENGTH_SHORT).show();
                });
    }

    private void displayContent(MovieDetail movieDetail) {
        mInitialVisibility.set(true);
        mOverview.set(movieDetail.overview());
        if (movieDetail.genres() != null) {
            StringBuilder stringBuilder = new StringBuilder();
            for (Genre genre : movieDetail.genres()) {
                stringBuilder.append(genre.name());
                stringBuilder.append(", ");
            }
            String genres = stringBuilder.toString();
            mGenres.set(mContext.getString(R.string.genres, genres.substring(0, genres.length() - 2)));
        }
        mLanguage.set(mContext.getString(R.string.language, movieDetail.original_language()));
        if (movieDetail.runtime() != null) {
            mDuration.set(mContext.getString(R.string.duration, movieDetail.runtime()));
        }
    }
}
