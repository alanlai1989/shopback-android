package com.shopback.interviewassignment.vivmer.interactor;

import com.shopback.interviewassignment.network.api.MovieAPI;
import com.shopback.interviewassignment.vivmer.entity.MovieDetail;

import io.reactivex.Observable;

public class MovieDetailInteractor extends BaseInteractor {
    private final MovieAPI mApi;

    public MovieDetailInteractor(final MovieAPI api) {
        mApi = api;
    }

    public Observable<MovieDetail> getMovieList(long movieId) {
        return mApi.getMovieDetail(movieId, API_KEY);
    }
}