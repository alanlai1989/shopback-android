package com.shopback.interviewassignment.vivmer.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import com.shopback.interviewassignment.CoreApplication;
import com.shopback.interviewassignment.R;
import com.shopback.interviewassignment.util.Logger;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;

    protected abstract int getLayoutResource();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreApplication.getInstance()
                .getApplicationComponent()
                .inject(this);
        onSetContentView();
        setRxJavaErrorHandler();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onSetContentView() {
        setContentView(getLayoutResource());
    }

    public void dismissProgressDialog() {
        runOnUiThread(() -> {
            if (!isFinishing() && progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        });
    }

    public synchronized void showProgressDialog() {
        runOnUiThread(() -> {
            try {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                progressDialog = ProgressDialog.show(this, "", getString(R.string.loading_text), true);
                progressDialog.setCancelable(true);
                progressDialog.setCanceledOnTouchOutside(false);
            } catch (Exception e) {
                Logger.logException(e);
            }
        });
    }

    private void setRxJavaErrorHandler() {
        if (RxJavaPlugins.getErrorHandler() != null && !RxJavaPlugins.isLockdown()) {
            RxJavaPlugins.setErrorHandler(Logger::logException);
        }
    }
}