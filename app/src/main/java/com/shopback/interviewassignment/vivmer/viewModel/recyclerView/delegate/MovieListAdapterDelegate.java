package com.shopback.interviewassignment.vivmer.viewModel.recyclerView.delegate;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shopback.interviewassignment.R;
import com.shopback.interviewassignment.databinding.ViewMovieListItemBinding;
import com.shopback.interviewassignment.vivmer.viewModel.MovieListItemViewModel;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MovieListAdapterDelegate extends ListAdapterDelegate<MovieListItemViewModel, RecyclerViewItemViewModel, MovieListAdapterDelegate.ItemViewHolder> {
    public MovieListAdapterDelegate(final LayoutInflater layoutInflater) {
        super(layoutInflater);
    }

    @Override
    protected boolean isForViewType(@NonNull final RecyclerViewItemViewModel item, final List<RecyclerViewItemViewModel> items, final int position) {
        return item instanceof MovieListItemViewModel;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent) {
        return new ItemViewHolder(mLayoutInflater.inflate(R.layout.view_movie_list_item, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull final MovieListItemViewModel item, @NonNull final ItemViewHolder viewHolder) {
        final ViewMovieListItemBinding binding = viewHolder.mBinding;
        binding.setViewModel(item);
        binding.executePendingBindings();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        ViewMovieListItemBinding mBinding;

        ItemViewHolder(final View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }
    }
}