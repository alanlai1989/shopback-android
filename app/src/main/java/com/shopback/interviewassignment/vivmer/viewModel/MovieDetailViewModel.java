package com.shopback.interviewassignment.vivmer.viewModel;

import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

public interface MovieDetailViewModel extends MovieListItemViewModel {
    ObservableBoolean getInitialVisibility();

    ObservableField<String> getOverview();

    ObservableField<String> getGenres();

    ObservableField<String> getLanguage();

    ObservableField<String> getDuration();

    void onBookClicked(View view);
}