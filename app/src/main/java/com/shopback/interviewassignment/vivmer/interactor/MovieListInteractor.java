package com.shopback.interviewassignment.vivmer.interactor;

import com.shopback.interviewassignment.network.api.MovieAPI;
import com.shopback.interviewassignment.network.api.pojo.response.MovieListResponse;
import com.shopback.interviewassignment.vivmer.entity.MovieList;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class MovieListInteractor extends BaseInteractor {
    private final MovieAPI mApi;

    public MovieListInteractor(final MovieAPI api) {
        mApi = api;
    }

    public Observable<List<MovieList>> getMovieList(int page) {
        return mApi.getMovieList(API_KEY, getTodayDate(), RELEASE_DATE_DESCENDING, page)
                .flatMap((Function<MovieListResponse, ObservableSource<List<MovieList>>>) movieListResponse -> Observable.just(movieListResponse.results()));
    }
}