package com.shopback.interviewassignment.vivmer.entity;

import android.os.Parcel;

import com.google.gson.JsonSyntaxException;
import com.shopback.interviewassignment.util.GsonUtil;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

@Gson.TypeAdapters
@Value.Immutable
public abstract class MovieList implements BaseMovie {
    // ----- Boilerplate for Parcelable ----- //
    public static final Creator<MovieList> CREATOR = new Creator<MovieList>() {
        @Override
        public MovieList createFromParcel(final Parcel parcel) {
            return fromJsonString(parcel.readString());
        }

        @Override
        public MovieList[] newArray(final int i) {
            return new MovieList[0];
        }
    };

    @Override
    public void writeToParcel(final Parcel parcel, final int flag) {
        parcel.writeString(toJsonString());
    }

    public String toJsonString() {
        return GsonUtil.getGson().toJson(this, MovieList.class);
    }

    public static MovieList fromJsonString(final String json) {
        try {
            return GsonUtil.getGson().fromJson(json, MovieList.class);
        } catch (final JsonSyntaxException e) {
            return null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }
}