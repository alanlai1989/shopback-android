package com.shopback.interviewassignment.vivmer.viewModel;

import android.content.Context;

import com.shopback.interviewassignment.util.Logger;
import com.shopback.interviewassignment.vivmer.entity.MovieList;
import com.shopback.interviewassignment.vivmer.interactor.MovieListInteractor;
import com.shopback.interviewassignment.vivmer.router.MovieListRouter;
import com.shopback.interviewassignment.vivmer.view.MovieListActivity;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.LoadingViewModel;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MovieListViewModelImpl implements MovieListViewModel {
    private final Context             mContext;
    private final MovieListInteractor mInteractor;
    private final MovieListRouter     mRouter;

    private final LoadingViewModel  mLoadingViewModel = new LoadingViewModel();
    private final ObservableBoolean mRefreshing       = new ObservableBoolean(true);
    private final ObservableBoolean mShowRefresh      = new ObservableBoolean(true);

    private ObservableArrayList<RecyclerViewItemViewModel> mViewModels;

    public MovieListViewModelImpl(final Context context,
                                  final MovieListInteractor interactor,
                                  final MovieListRouter router) {
        mContext = context;
        mInteractor = interactor;
        mRouter = router;
        mViewModels = new ObservableArrayList<>();
        loadMore(1);
    }

    @Override
    public ObservableArrayList<RecyclerViewItemViewModel> getViewModels() {
        return mViewModels;
    }

    @Override
    public Observable<List<? extends RecyclerViewItemViewModel>> loadPage(final int page) {
        mRefreshing.set(true);
        mShowRefresh.set(page == 1);
        if (page == 1) {
            mViewModels.clear();
        }
        else {
            for (RecyclerViewItemViewModel viewModel : mViewModels) {
                if (viewModel instanceof LoadingViewModel) {
                    return retrieveData(page);
                }
            }
            mViewModels.add(mLoadingViewModel);
        }
        return retrieveData(page);
    }

    @Override
    public SwipeRefreshLayout.OnRefreshListener getOnRefreshListener() {
        return () -> loadMore(1);
    }

    @Override
    public ObservableBoolean getRefreshing() {
        return mRefreshing;
    }

    @Override
    public ObservableBoolean getShouldShowRefresh() {
        return mShowRefresh;
    }

    @Override
    public void loadMore(final int page) {
        if (page == 1) {
            ((MovieListActivity) mContext).resetScrollCounter();
        }
        loadPage(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(recyclerViewItemViewModels -> {
                    getViewModels().addAll(recyclerViewItemViewModels);
                }, Logger::logException);
    }

    private Observable<List<? extends RecyclerViewItemViewModel>> retrieveData(final int page) {
        return mInteractor.getMovieList(page)
                .flatMap((Function<List<MovieList>, ObservableSource<List<? extends RecyclerViewItemViewModel>>>) movieLists -> {
                    List<MovieListItemViewModel> viewModelList = new ArrayList<>();
                    if (!Objects.requireNonNull(movieLists).isEmpty()) {
                        for (MovieList movieList : movieLists) {
                            MovieListItemViewModel viewModel = new MovieListItemViewModelImpl(mContext, mRouter, movieList);
                            viewModelList.add(viewModel);
                        }
                    }
                    return Observable.just(viewModelList);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(recyclerViewItemViewModels -> {
                    mRefreshing.set(false);
                    mShowRefresh.set(false);
                    mViewModels.remove(mLoadingViewModel);
                })
                .doOnError(throwable -> {
                    Logger.logException(throwable);
                    mViewModels.remove(mLoadingViewModel);
                    mShowRefresh.set(false);
                });
    }
}