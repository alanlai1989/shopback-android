package com.shopback.interviewassignment.vivmer.entity;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

@Gson.TypeAdapters
@Value.Immutable
public interface Genre {
    String name();
}