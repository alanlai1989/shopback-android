package com.shopback.interviewassignment.vivmer.view;

import android.os.Bundle;
import android.view.LayoutInflater;

import com.shopback.interviewassignment.CoreApplication;
import com.shopback.interviewassignment.R;
import com.shopback.interviewassignment.dagger.module.MovieListModule;
import com.shopback.interviewassignment.databinding.ActivityMovieListBinding;
import com.shopback.interviewassignment.listener.EndlessRecyclerViewScrollListener;
import com.shopback.interviewassignment.vivmer.interactor.MovieListInteractor;
import com.shopback.interviewassignment.vivmer.router.MovieListRouter;
import com.shopback.interviewassignment.vivmer.viewModel.MovieListViewModel;
import com.shopback.interviewassignment.vivmer.viewModel.MovieListViewModelImpl;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewAdapter;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewDividerDecoration;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.RecyclerViewItemViewModel;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.delegate.MovieListAdapterDelegate;
import com.shopback.interviewassignment.vivmer.viewModel.recyclerView.delegate.RecyclerViewLoadingAdapterDelegate;
import com.hannesdorfmann.adapterdelegates2.AdapterDelegatesManager;

import java.util.List;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

public class MovieListActivity extends BaseActivity {
    @Inject
    MovieListInteractor mInteractor;

    @Inject
    MovieListRouter mRouter;

    private ActivityMovieListBinding          mBinding;
    private MovieListViewModel                mViewModel;
    private EndlessRecyclerViewScrollListener mListener;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_movie_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CoreApplication.getInstance()
                .getApplicationComponent()
                .plus(new MovieListModule(this))
                .inject(this);
        bindData();
        setup();
        setupRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.actionBar.toolBar.getBackground().setAlpha(255);
    }

    private void bindData() {
        mViewModel = new MovieListViewModelImpl(this, mInteractor, mRouter);
        mBinding = DataBindingUtil.setContentView(this, getLayoutResource());
        mBinding.setViewModel(mViewModel);
    }

    private void setup() {
        setSupportActionBar(mBinding.actionBar.toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(R.string.movie_list_title);
    }

    private void setupRecyclerView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        AdapterDelegatesManager<List<RecyclerViewItemViewModel>> manager = new AdapterDelegatesManager<>();
        manager.addDelegate(new RecyclerViewLoadingAdapterDelegate(inflater));
        manager.addDelegate(new MovieListAdapterDelegate(inflater));
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(manager, mViewModel.getViewModels());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(final int page) {
                mBinding.recyclerView.post(() -> mViewModel.loadMore(page));
            }
        };
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.recyclerView.setAdapter(adapter);
        mBinding.recyclerView.addItemDecoration(new RecyclerViewDividerDecoration(this, R.drawable.divider_transparent_small));
        mBinding.recyclerView.addOnScrollListener(mListener);
    }

    public void resetScrollCounter() {
        if (mListener != null) {
            mListener.reset();
        }
    }
}
