package com.shopback.interviewassignment.vivmer.viewModel.recyclerView;

import java.util.List;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableBoolean;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.reactivex.Observable;

public interface RecyclerViewViewModel {
    ObservableArrayList<RecyclerViewItemViewModel> getViewModels();

    Observable<List<? extends RecyclerViewItemViewModel>> loadPage(int page);

    SwipeRefreshLayout.OnRefreshListener getOnRefreshListener();

    ObservableBoolean getRefreshing();

    ObservableBoolean getShouldShowRefresh();
}