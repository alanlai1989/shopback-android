package com.shopback.interviewassignment.vivmer.entity;

import android.os.Parcelable;

import androidx.annotation.Nullable;

public interface BaseMovie extends Parcelable {
    long id();

    String title();

    double popularity();

    @Nullable
    String poster_path();

    @Nullable
    String backdrop_path();
}