package com.shopback.interviewassignment.vivmer.viewModel.recyclerView.delegate;

import android.view.LayoutInflater;

import com.hannesdorfmann.adapterdelegates2.AbsListItemAdapterDelegate;

import androidx.recyclerview.widget.RecyclerView;

public abstract class ListAdapterDelegate<I extends T, T, VH extends RecyclerView.ViewHolder> extends AbsListItemAdapterDelegate<I, T, VH> {
    protected final LayoutInflater mLayoutInflater;

    public ListAdapterDelegate(final LayoutInflater layoutInflater) {
        mLayoutInflater = layoutInflater;
    }
}