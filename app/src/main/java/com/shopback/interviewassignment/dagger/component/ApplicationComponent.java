package com.shopback.interviewassignment.dagger.component;

import android.content.Context;

import com.shopback.interviewassignment.CoreApplication;
import com.shopback.interviewassignment.dagger.module.APIModule;
import com.shopback.interviewassignment.dagger.module.ApplicationModule;
import com.shopback.interviewassignment.dagger.module.MovieDetailModule;
import com.shopback.interviewassignment.dagger.module.MovieListModule;
import com.shopback.interviewassignment.dagger.module.NetworkModule;
import com.shopback.interviewassignment.vivmer.view.BaseActivity;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, APIModule.class})
public interface ApplicationComponent {
    CoreApplication getApplication();

    Retrofit getRetrofit();

    Context getApplicationContext();

    Gson getGson();

    void inject(CoreApplication application);

    void inject(BaseActivity baseActivity);

    MovieListComponent plus(MovieListModule movieListModule);

    MovieDetailComponent plus(MovieDetailModule movieDetailModule);
}
