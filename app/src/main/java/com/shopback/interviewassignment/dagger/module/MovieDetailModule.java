package com.shopback.interviewassignment.dagger.module;

import com.shopback.interviewassignment.network.api.MovieAPI;
import com.shopback.interviewassignment.vivmer.interactor.MovieDetailInteractor;
import com.shopback.interviewassignment.vivmer.router.MovieDetailRouter;
import com.shopback.interviewassignment.vivmer.view.BaseActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class MovieDetailModule {
    private final BaseActivity mActivity;

    public MovieDetailModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    MovieDetailInteractor provideMovieDetailInteractor(final MovieAPI api) {
        return new MovieDetailInteractor(api);
    }

    @Provides
    MovieDetailRouter provideMovieDetailRouter() {
        return new MovieDetailRouter(mActivity);
    }
}