package com.shopback.interviewassignment.dagger.component;

import com.shopback.interviewassignment.dagger.module.MovieDetailModule;
import com.shopback.interviewassignment.vivmer.view.MovieDetailActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {MovieDetailModule.class})
public interface MovieDetailComponent {
    void inject(MovieDetailActivity activity);
}