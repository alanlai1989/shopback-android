package com.shopback.interviewassignment.dagger.module;

import com.shopback.interviewassignment.network.api.MovieAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class APIModule {
    @Provides
    @Singleton
    MovieAPI provideEngineerListAPI(Retrofit retrofit) {
        return retrofit.create(MovieAPI.class);
    }
}