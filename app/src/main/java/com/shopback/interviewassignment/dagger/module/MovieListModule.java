package com.shopback.interviewassignment.dagger.module;

import com.shopback.interviewassignment.network.api.MovieAPI;
import com.shopback.interviewassignment.vivmer.interactor.MovieListInteractor;
import com.shopback.interviewassignment.vivmer.router.MovieListRouter;
import com.shopback.interviewassignment.vivmer.view.BaseActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class MovieListModule {
    private final BaseActivity mActivity;

    public MovieListModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    MovieListInteractor provideMovieListInteractor(final MovieAPI api) {
        return new MovieListInteractor(api);
    }

    @Provides
    MovieListRouter provideMovieListRouter() {
        return new MovieListRouter(mActivity);
    }
}