package com.shopback.interviewassignment.dagger.module;

import android.content.Context;

import com.shopback.interviewassignment.network.BaseOkHttp3RequestInterceptor;
import com.shopback.interviewassignment.network.BaseOkHttp3ResponseInterceptor;
import com.shopback.interviewassignment.network.CustomHttpLoggingInterceptor;
import com.shopback.interviewassignment.network.ErrorRequestInterceptor;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    @Singleton
    @Provides
    BaseOkHttp3RequestInterceptor provideBaseOkHttp3RequestInterceptor() {
        return new BaseOkHttp3RequestInterceptor();
    }

    @Singleton
    @Provides
    BaseOkHttp3ResponseInterceptor provideBaseOkHttp3ResponseInterceptor() {
        return new BaseOkHttp3ResponseInterceptor();
    }

    @Singleton
    @Provides
    ErrorRequestInterceptor provideErrorRequestInterceptor() {
        return new ErrorRequestInterceptor();
    }

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(final Cache cache,
                                     final BaseOkHttp3RequestInterceptor baseInterceptor,
                                     final BaseOkHttp3ResponseInterceptor responseInterceptor,
                                     final ErrorRequestInterceptor errorInterceptor) {
        final long DEFAULT_CONNECT_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(10);
        final long DEFAULT_READ_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(30);
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(baseInterceptor)
                .connectTimeout(DEFAULT_CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .readTimeout(DEFAULT_READ_TIMEOUT_MS, TimeUnit.MILLISECONDS);
        builder.addInterceptor(errorInterceptor);
        builder.addInterceptor(responseInterceptor);
        CustomHttpLoggingInterceptor loggingInterceptor = new CustomHttpLoggingInterceptor();
        loggingInterceptor.setLevel(CustomHttpLoggingInterceptor.Level.BODY);
        builder.addNetworkInterceptor(loggingInterceptor);
        return builder.build();
    }

    @Singleton
    @Provides
    Cache provideCache(Context context) {
        return new Cache(context.getCacheDir(), 10 * 1024 * 1024);
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(final OkHttpClient okHttpClient,
                             final Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl("http://api.themoviedb.org/")
                .build();
    }
}
