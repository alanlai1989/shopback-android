package com.shopback.interviewassignment.dagger.component;

import com.shopback.interviewassignment.dagger.module.MovieListModule;
import com.shopback.interviewassignment.vivmer.view.MovieListActivity;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {MovieListModule.class})
public interface MovieListComponent {
    void inject(MovieListActivity activity);
}