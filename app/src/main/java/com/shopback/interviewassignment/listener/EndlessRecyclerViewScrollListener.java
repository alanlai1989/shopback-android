package com.shopback.interviewassignment.listener;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {
    private final LinearLayoutManager mLayoutManager;

    // The current offset index of data you have loaded
    private int     currentPage   = 1;
    // True if we are still waiting for the last set of data to load.
    private boolean loading       = true;
    // The total number of items in the data set after the last load
    private int     previousTotal = 0;

    protected EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager) {
        mLayoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        final int visibleItemCount = recyclerView.getChildCount();
        final int totalItemCount = mLayoutManager.getItemCount();
        final int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
        final int visibleThreshold = 20;
        if (loading) {
            if (totalItemCount > previousTotal && totalItemCount - previousTotal == visibleThreshold) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached do something
            currentPage++;
            onLoadMore(currentPage);
            loading = true;
        }
    }

    public void reset() {
        currentPage = 1;
        previousTotal = 0;
    }

    public abstract void onLoadMore(int page);
}