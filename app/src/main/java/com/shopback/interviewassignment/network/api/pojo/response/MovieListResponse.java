package com.shopback.interviewassignment.network.api.pojo.response;

import com.shopback.interviewassignment.vivmer.entity.MovieList;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

import java.util.List;

import androidx.annotation.Nullable;

@Gson.TypeAdapters
@Value.Immutable
public interface MovieListResponse {
    @Nullable
    List<MovieList> results();
}