package com.shopback.interviewassignment.network.api;

import com.shopback.interviewassignment.network.api.pojo.response.MovieListResponse;
import com.shopback.interviewassignment.vivmer.entity.MovieDetail;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieAPI {
    @GET("3/discover/movie")
    Observable<MovieListResponse> getMovieList(@Query("api_key") String apiKey,
                                               @Query("primary_release_date.lte") String today,
                                               @Query("sort_by") String sortBy,
                                               @Query("page") int page);

    @GET("3/movie/{movie_id}")
    Observable<MovieDetail> getMovieDetail(@Path("movie_id") long id,
                                           @Query("api_key") String apiKey);
}