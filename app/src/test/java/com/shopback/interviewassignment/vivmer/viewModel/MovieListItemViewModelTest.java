package com.shopback.interviewassignment.vivmer.viewModel;

import android.content.Context;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import com.shopback.interviewassignment.vivmer.entity.BaseMovie;
import com.shopback.interviewassignment.vivmer.router.MovieListRouter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MovieListItemViewModelTest {
    private MovieListItemViewModel mViewModel;

    @Mock
    BaseMovie mBaseMovie;

    @Mock
    Context mContext;

    @Mock
    MovieListRouter mRouter;

    @Test
    public void MovieListItemViewModel_getTitle() {
        mViewModel = new MovieListItemViewModelImpl(mContext, mRouter, mBaseMovie);
        when(mViewModel.getTitle())
                .thenReturn("Title");
        assertThat("get title is Title", mViewModel.getTitle(), is("Title"));
    }

    @Test
    public void MovieListItemViewModel_getTitleEmpty() {
        mViewModel = new MovieListItemViewModelImpl(mContext, mRouter, null);
        assertThat("get title is empty string", mViewModel.getTitle(), is(""));
    }

    @Test
    public void MovieListItemViewModel_getPopularity() {
        mViewModel = new MovieListItemViewModelImpl(mContext, mRouter, mBaseMovie);
        when(mViewModel.getPopularity())
                .thenReturn("Popularity: 0.12345");
        assertThat("get popularity is Popularity: 0.12345", mViewModel.getPopularity(), is("Popularity: 0.12345"));
    }

    @Test
    public void MovieListItemViewModel_getPopularityEmpty() {
        mViewModel = new MovieListItemViewModelImpl(mContext, mRouter, null);
        assertThat("get popularity is empty string", mViewModel.getPopularity(), is(""));
    }
}